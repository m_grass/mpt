.. mpt documentation master file, created by
   sphinx-quickstart on Fri Jun 22 12:39:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mpt's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


mpt package
===========


Submodules
----------

mpt.main module
---------------

.. automodule:: mpt.main
    :members:
    :undoc-members:
    :show-inheritance:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

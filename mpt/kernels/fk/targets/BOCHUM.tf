KPL/FK
 
   FILE: /home/flp/mpt/mpt/mpt/kernels/fk/targets/BOCHUM.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-06-20T16:33:52
   PINPOINT DEFINITIONS FILE: /home/flp/mpt/mpt/mpt/kernels/spk/spk_target_setup
   PINPOINT PCK FILE:         /home/flp/mpt/mpt/mpt/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/mpt/mpt/mpt/kernels/spk/targets/BOCHUM.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'BOCHUM'
   NAIF_BODY_CODE                      += 399264
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame BOCHUM_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame BOCHUM_TOPO is centered at the
      site BOCHUM, which has Cartesian coordinates
 
         X (km):                  0.3948779780937E+04
         Y (km):                  0.5000008520432E+03
         Z (km):                  0.4967170766512E+04
 
      and planetodetic coordinates
 
         Longitude (deg):         7.2164800000000
         Latitude  (deg):        51.4816500000000
         Altitude   (km):         0.1019999999989E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_BOCHUM_TOPO                   =  1399264
   FRAME_1399264_NAME                  =  'BOCHUM_TOPO'
   FRAME_1399264_CLASS                 =  4
   FRAME_1399264_CLASS_ID              =  1399264
   FRAME_1399264_CENTER                =  399264
 
   OBJECT_399264_FRAME                 =  'BOCHUM_TOPO'
 
   TKFRAME_1399264_RELATIVE            =  'ITRF93'
   TKFRAME_1399264_SPEC                =  'ANGLES'
   TKFRAME_1399264_UNITS               =  'DEGREES'
   TKFRAME_1399264_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399264_ANGLES              =  (   -7.2164800000000,
                                             -38.5183500000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/mpt/mpt/mpt/kernels/spk/spk_target_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'BOCHUM' )
BOCHUM_CENTER       = 399
BOCHUM_FRAME        = 'ITRF93'
BOCHUM_IDCODE       = 399264
BOCHUM_LATLON       = ( 51.48165 7.21648 0.102 )
BOCHUM_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
BOCHUM_UP       = 'Z'
BOCHUM_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/targets/SAN_FRANCISCO.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-06-13T11:02:39
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_target_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/SAN_FRANCISCO.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'SAN_FRANCISCO'
   NAIF_BODY_CODE                      += 399254
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame SAN_FRANCISCO_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame SAN_FRANCISCO_TOPO is centered at the
      site SAN_FRANCISCO, which has Cartesian coordinates
 
         X (km):                 -0.2706186938433E+04
         Y (km):                 -0.4261075242061E+04
         Z (km):                  0.3885745014478E+04
 
      and planetodetic coordinates
 
         Longitude (deg):      -122.4194200000000
         Latitude  (deg):        37.7749300000000
         Altitude   (km):         0.2800000000024E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_SAN_FRANCISCO_TOPO            =  1399254
   FRAME_1399254_NAME                  =  'SAN_FRANCISCO_TOPO'
   FRAME_1399254_CLASS                 =  4
   FRAME_1399254_CLASS_ID              =  1399254
   FRAME_1399254_CENTER                =  399254
 
   OBJECT_399254_FRAME                 =  'SAN_FRANCISCO_TOPO'
 
   TKFRAME_1399254_RELATIVE            =  'ITRF93'
   TKFRAME_1399254_SPEC                =  'ANGLES'
   TKFRAME_1399254_UNITS               =  'DEGREES'
   TKFRAME_1399254_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399254_ANGLES              =  ( -237.5805800000000,
                                             -52.2250700000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_target_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'SAN_FRANCISCO' )
SAN_FRANCISCO_CENTER       = 399
SAN_FRANCISCO_FRAME        = 'ITRF93'
SAN_FRANCISCO_IDCODE       = 399254
SAN_FRANCISCO_LATLON       = ( 37.77493 -122.41942 0.028 )
SAN_FRANCISCO_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
SAN_FRANCISCO_UP       = 'Z'
SAN_FRANCISCO_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

KPL/FK
 
   FILE: /home/flp/mpt/mpt/mpt/kernels/fk/targets/BAYREUTH.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-06-20T16:46:48
   PINPOINT DEFINITIONS FILE: /home/flp/mpt/mpt/mpt/kernels/spk/spk_target_setup
   PINPOINT PCK FILE:         /home/flp/mpt/mpt/mpt/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/mpt/mpt/mpt/kernels/spk/targets/BAYREUTH.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'BAYREUTH'
   NAIF_BODY_CODE                      += 399271
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame BAYREUTH_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame BAYREUTH_TOPO is centered at the
      site BAYREUTH, which has Cartesian coordinates
 
         X (km):                  0.4028832203758E+04
         Y (km):                  0.8254566055584E+03
         Z (km):                  0.4859314738071E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        11.5789300000000
         Latitude  (deg):        49.9478200000000
         Altitude   (km):         0.3379999999991E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_BAYREUTH_TOPO                 =  1399271
   FRAME_1399271_NAME                  =  'BAYREUTH_TOPO'
   FRAME_1399271_CLASS                 =  4
   FRAME_1399271_CLASS_ID              =  1399271
   FRAME_1399271_CENTER                =  399271
 
   OBJECT_399271_FRAME                 =  'BAYREUTH_TOPO'
 
   TKFRAME_1399271_RELATIVE            =  'ITRF93'
   TKFRAME_1399271_SPEC                =  'ANGLES'
   TKFRAME_1399271_UNITS               =  'DEGREES'
   TKFRAME_1399271_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399271_ANGLES              =  (  -11.5789300000000,
                                             -40.0521800000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/mpt/mpt/mpt/kernels/spk/spk_target_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'BAYREUTH' )
BAYREUTH_CENTER       = 399
BAYREUTH_FRAME        = 'ITRF93'
BAYREUTH_IDCODE       = 399271
BAYREUTH_LATLON       = ( 49.94782 11.57893 0.338 )
BAYREUTH_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
BAYREUTH_UP       = 'Z'
BAYREUTH_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

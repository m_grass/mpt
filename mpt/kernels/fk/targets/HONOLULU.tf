KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/targets/HONOLULU.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-16T17:34:19
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_target_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/HONOLULU.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'HONOLULU'
   NAIF_BODY_CODE                      += 399252
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame HONOLULU_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame HONOLULU_TOPO is centered at the
      site HONOLULU, which has Cartesian coordinates
 
         X (km):                 -0.5506415863265E+04
         Y (km):                 -0.2240590005030E+04
         Z (km):                  0.2303095520879E+04
 
      and planetodetic coordinates
 
         Longitude (deg):      -157.8583300000000
         Latitude  (deg):        21.3069400000000
         Altitude   (km):         0.1800000000242E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_HONOLULU_TOPO                 =  1399252
   FRAME_1399252_NAME                  =  'HONOLULU_TOPO'
   FRAME_1399252_CLASS                 =  4
   FRAME_1399252_CLASS_ID              =  1399252
   FRAME_1399252_CENTER                =  399252
 
   OBJECT_399252_FRAME                 =  'HONOLULU_TOPO'
 
   TKFRAME_1399252_RELATIVE            =  'ITRF93'
   TKFRAME_1399252_SPEC                =  'ANGLES'
   TKFRAME_1399252_UNITS               =  'DEGREES'
   TKFRAME_1399252_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399252_ANGLES              =  ( -202.1416700000000,
                                             -68.6930600000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_target_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'HONOLULU' )
HONOLULU_CENTER       = 399
HONOLULU_FRAME        = 'ITRF93'
HONOLULU_IDCODE       = 399252
HONOLULU_LATLON       = ( 21.30694 -157.85833 0.018 )
HONOLULU_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
HONOLULU_UP       = 'Z'
HONOLULU_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

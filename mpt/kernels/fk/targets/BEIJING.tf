KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/targets/BEIJING.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-06-13T13:54:37
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_target_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/BEIJING.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'BEIJING'
   NAIF_BODY_CODE                      += 399256
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame BEIJING_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame BEIJING_TOPO is centered at the
      site BEIJING, which has Cartesian coordinates
 
         X (km):                 -0.2178206916115E+04
         Y (km):                  0.4388502088891E+04
         Z (km):                  0.4070143658984E+04
 
      and planetodetic coordinates
 
         Longitude (deg):       116.3972300000000
         Latitude  (deg):        39.9075000000000
         Altitude   (km):         0.4899999999877E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_BEIJING_TOPO                  =  1399256
   FRAME_1399256_NAME                  =  'BEIJING_TOPO'
   FRAME_1399256_CLASS                 =  4
   FRAME_1399256_CLASS_ID              =  1399256
   FRAME_1399256_CENTER                =  399256
 
   OBJECT_399256_FRAME                 =  'BEIJING_TOPO'
 
   TKFRAME_1399256_RELATIVE            =  'ITRF93'
   TKFRAME_1399256_SPEC                =  'ANGLES'
   TKFRAME_1399256_UNITS               =  'DEGREES'
   TKFRAME_1399256_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399256_ANGLES              =  ( -116.3972300000000,
                                             -50.0925000000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_target_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'BEIJING' )
BEIJING_CENTER       = 399
BEIJING_FRAME        = 'ITRF93'
BEIJING_IDCODE       = 399256
BEIJING_LATLON       = ( 39.9075 116.39723 0.049 )
BEIJING_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
BEIJING_UP       = 'Z'
BEIJING_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

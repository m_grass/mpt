KPL/FK
 
   FILE: /home/flp/mpt/mpt/mpt/kernels/fk/targets/HOF.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-06-20T17:36:21
   PINPOINT DEFINITIONS FILE: /home/flp/mpt/mpt/mpt/kernels/spk/spk_target_setup
   PINPOINT PCK FILE:         /home/flp/mpt/mpt/mpt/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/mpt/mpt/mpt/kernels/spk/targets/HOF.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'HOF'
   NAIF_BODY_CODE                      += 399296
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame HOF_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame HOF_TOPO is centered at the
      site HOF, which has Cartesian coordinates
 
         X (km):                  0.3993548838719E+04
         Y (km):                  0.8424907358591E+03
         Z (km):                  0.4885470531774E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        11.9126100000000
         Latitude  (deg):        50.3129700000000
         Altitude   (km):         0.4910000000003E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_HOF_TOPO                      =  1399296
   FRAME_1399296_NAME                  =  'HOF_TOPO'
   FRAME_1399296_CLASS                 =  4
   FRAME_1399296_CLASS_ID              =  1399296
   FRAME_1399296_CENTER                =  399296
 
   OBJECT_399296_FRAME                 =  'HOF_TOPO'
 
   TKFRAME_1399296_RELATIVE            =  'ITRF93'
   TKFRAME_1399296_SPEC                =  'ANGLES'
   TKFRAME_1399296_UNITS               =  'DEGREES'
   TKFRAME_1399296_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399296_ANGLES              =  (  -11.9126100000000,
                                             -39.6870300000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/mpt/mpt/mpt/kernels/spk/spk_target_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'HOF' )
HOF_CENTER       = 399
HOF_FRAME        = 'ITRF93'
HOF_IDCODE       = 399296
HOF_LATLON       = ( 50.31297 11.91261 0.491 )
HOF_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
HOF_UP       = 'Z'
HOF_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

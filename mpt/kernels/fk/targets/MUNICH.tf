KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/out/MUNICH.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-03T16:44:59
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/MUNICH.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'MUNICH'
   NAIF_BODY_CODE                      += 399238
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame MUNICH_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame MUNICH_TOPO is centered at the
      site MUNICH, which has Cartesian coordinates
 
         X (km):                  0.4177959975394E+04
         Y (km):                  0.8557496252802E+03
         Z (km):                  0.4727477666907E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        11.5754900000000
         Latitude  (deg):        48.1374300000000
         Altitude   (km):         0.5240000000010E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_MUNICH_TOPO                   =  1399238
   FRAME_1399238_NAME                  =  'MUNICH_TOPO'
   FRAME_1399238_CLASS                 =  4
   FRAME_1399238_CLASS_ID              =  1399238
   FRAME_1399238_CENTER                =  399238
 
   OBJECT_399238_FRAME                 =  'MUNICH_TOPO'
 
   TKFRAME_1399238_RELATIVE            =  'ITRF93'
   TKFRAME_1399238_SPEC                =  'ANGLES'
   TKFRAME_1399238_UNITS               =  'DEGREES'
   TKFRAME_1399238_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399238_ANGLES              =  (  -11.5754900000000,
                                             -41.8625700000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'MUNICH' )
MUNICH_CENTER       = 399
MUNICH_FRAME        = 'ITRF93'
MUNICH_IDCODE       = 399238
MUNICH_LATLON       = ( 48.13743 11.57549 0.524 )
MUNICH_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
MUNICH_UP       = 'Z'
MUNICH_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/out/MADRID.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-04T15:31:51
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/MADRID.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'MADRID'
   NAIF_BODY_CODE                      += 399240
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame MADRID_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame MADRID_TOPO is centered at the
      site MADRID, which has Cartesian coordinates
 
         X (km):                  0.4853206591424E+04
         Y (km):                 -0.3140605427380E+03
         Z (km):                  0.4113735793878E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        -3.7025600000000
         Latitude  (deg):        40.4165000000000
         Altitude   (km):         0.6650000000015E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_MADRID_TOPO                   =  1399240
   FRAME_1399240_NAME                  =  'MADRID_TOPO'
   FRAME_1399240_CLASS                 =  4
   FRAME_1399240_CLASS_ID              =  1399240
   FRAME_1399240_CENTER                =  399240
 
   OBJECT_399240_FRAME                 =  'MADRID_TOPO'
 
   TKFRAME_1399240_RELATIVE            =  'ITRF93'
   TKFRAME_1399240_SPEC                =  'ANGLES'
   TKFRAME_1399240_UNITS               =  'DEGREES'
   TKFRAME_1399240_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399240_ANGLES              =  ( -356.2974400000000,
                                             -49.5835000000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'MADRID' )
MADRID_CENTER       = 399
MADRID_FRAME        = 'ITRF93'
MADRID_IDCODE       = 399240
MADRID_LATLON       = ( 40.4165 -3.70256 0.665 )
MADRID_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
MADRID_UP       = 'Z'
MADRID_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

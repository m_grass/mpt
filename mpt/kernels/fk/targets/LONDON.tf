KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/out/LONDON.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-06T14:43:33
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/LONDON.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'LONDON'
   NAIF_BODY_CODE                      += 399243
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame LONDON_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame LONDON_TOPO is centered at the
      site LONDON, which has Cartesian coordinates
 
         X (km):                  0.3977911505336E+04
         Y (km):                 -0.8729847118253E+01
         Z (km):                  0.4968972426051E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        -0.1257400000000
         Latitude  (deg):        51.5085300000000
         Altitude   (km):         0.2500000000008E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_LONDON_TOPO                   =  1399243
   FRAME_1399243_NAME                  =  'LONDON_TOPO'
   FRAME_1399243_CLASS                 =  4
   FRAME_1399243_CLASS_ID              =  1399243
   FRAME_1399243_CENTER                =  399243
 
   OBJECT_399243_FRAME                 =  'LONDON_TOPO'
 
   TKFRAME_1399243_RELATIVE            =  'ITRF93'
   TKFRAME_1399243_SPEC                =  'ANGLES'
   TKFRAME_1399243_UNITS               =  'DEGREES'
   TKFRAME_1399243_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399243_ANGLES              =  ( -359.8742600000000,
                                             -38.4914700000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'LONDON' )
LONDON_CENTER       = 399
LONDON_FRAME        = 'ITRF93'
LONDON_IDCODE       = 399243
LONDON_LATLON       = ( 51.50853 -0.12574 0.025 )
LONDON_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
LONDON_UP       = 'Z'
LONDON_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

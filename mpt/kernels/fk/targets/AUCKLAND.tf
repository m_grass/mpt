KPL/FK
 
   FILE: /home/flp/mpt/mpt/mpt/kernels/fk/targets/AUCKLAND.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-06-25T13:36:13
   PINPOINT DEFINITIONS FILE: /home/flp/mpt/mpt/mpt/kernels/spk/spk_target_setup
   PINPOINT PCK FILE:         /home/flp/mpt/mpt/mpt/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/mpt/mpt/mpt/kernels/spk/targets/AUCKLAND.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'AUCKLAND'
   NAIF_BODY_CODE                      += 399299
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame AUCKLAND_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame AUCKLAND_TOPO is centered at the
      site AUCKLAND, which has Cartesian coordinates
 
         X (km):                 -0.5087650385046E+04
         Y (km):                  0.4659967057113E+03
         Z (km):                 -0.3805612956260E+04
 
      and planetodetic coordinates
 
         Longitude (deg):       174.7666700000000
         Latitude  (deg):       -36.8666700000000
         Altitude   (km):         0.7899999999976E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_AUCKLAND_TOPO                 =  1399299
   FRAME_1399299_NAME                  =  'AUCKLAND_TOPO'
   FRAME_1399299_CLASS                 =  4
   FRAME_1399299_CLASS_ID              =  1399299
   FRAME_1399299_CENTER                =  399299
 
   OBJECT_399299_FRAME                 =  'AUCKLAND_TOPO'
 
   TKFRAME_1399299_RELATIVE            =  'ITRF93'
   TKFRAME_1399299_SPEC                =  'ANGLES'
   TKFRAME_1399299_UNITS               =  'DEGREES'
   TKFRAME_1399299_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399299_ANGLES              =  ( -174.7666700000000,
                                            -126.8666700000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/mpt/mpt/mpt/kernels/spk/spk_target_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'AUCKLAND' )
AUCKLAND_CENTER       = 399
AUCKLAND_FRAME        = 'ITRF93'
AUCKLAND_IDCODE       = 399299
AUCKLAND_LATLON       = ( -36.86667 174.76667 0.079 )
AUCKLAND_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
AUCKLAND_UP       = 'Z'
AUCKLAND_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

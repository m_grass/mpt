KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/out/LOS_ANGELES.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-04T18:55:44
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/LOS_ANGELES.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'LOS_ANGELES'
   NAIF_BODY_CODE                      += 399242
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame LOS_ANGELES_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame LOS_ANGELES_TOPO is centered at the
      site LOS_ANGELES, which has Cartesian coordinates
 
         X (km):                 -0.2503392287936E+04
         Y (km):                 -0.4660272446838E+04
         Z (km):                  0.3551301634017E+04
 
      and planetodetic coordinates
 
         Longitude (deg):      -118.2436800000000
         Latitude  (deg):        34.0522300000000
         Altitude   (km):         0.9599999999919E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_LOS_ANGELES_TOPO              =  1399242
   FRAME_1399242_NAME                  =  'LOS_ANGELES_TOPO'
   FRAME_1399242_CLASS                 =  4
   FRAME_1399242_CLASS_ID              =  1399242
   FRAME_1399242_CENTER                =  399242
 
   OBJECT_399242_FRAME                 =  'LOS_ANGELES_TOPO'
 
   TKFRAME_1399242_RELATIVE            =  'ITRF93'
   TKFRAME_1399242_SPEC                =  'ANGLES'
   TKFRAME_1399242_UNITS               =  'DEGREES'
   TKFRAME_1399242_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399242_ANGLES              =  ( -241.7563200000000,
                                             -55.9477700000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'LOS_ANGELES' )
LOS_ANGELES_CENTER       = 399
LOS_ANGELES_FRAME        = 'ITRF93'
LOS_ANGELES_IDCODE       = 399242
LOS_ANGELES_LATLON       = ( 34.05223 -118.24368 0.096 )
LOS_ANGELES_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
LOS_ANGELES_UP       = 'Z'
LOS_ANGELES_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

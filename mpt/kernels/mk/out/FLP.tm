\begindata

PATH_VALUES = ( '/home/flp/H/mpt/mpt/kernels/' )

PATH_SYMBOLS = ( 'KERNELS' )

KERNELS_TO_LOAD = (

'/home/flp/H/mpt/mpt/kernels/lsk/naif/naif0012.tls', 
'/home/flp/H/mpt/mpt/kernels/spk/naif/de432s.bsp', 
'/home/flp/H/mpt/mpt/kernels/pck/naif/pck00010.tpc', 
'/home/flp/H/mpt/mpt/kernels/pck/naif/geophysical.ker', 
'/home/flp/H/mpt/mpt/kernels/pck/naif/earth_000101_180827_180605.bpc', 

'/home/flp/H/mpt/mpt/kernels/sclk/flp.tsc', 
'/home/flp/H/mpt/mpt/kernels/fk/FLP.tf', 
'/home/flp/H/mpt/mpt/kernels/ik/instruments.ti',

'/home/flp/H/mpt/mpt/kernels/fk/stations/stations.tf', 
'/home/flp/H/mpt/mpt/kernels/spk/stations/stations.bsp',

'/home/flp/H/mpt/mpt/kernels/spk/gmat/spk_gmat_merge_new.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/tle_output/spk_tle.bsp',

'/home/flp/H/mpt/mpt/kernels/spk/out/spk_master_merge_new.bsp',

'/home/flp/H/mpt/mpt/kernels/spk/instrument/instrument.bsp',

'/home/flp/H/mpt/mpt/kernels/ck/propagation/ck_prop_4_moon_pointing.bc',
'/home/flp/H/mpt/mpt/kernels/ck/out/ck_master.bc',

'/home/flp/H/mpt/mpt/kernels/fk/targets/AACHEN.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/AALEN.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/AREA_51.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/AUCKLAND.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/BARCELONA.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/BAYREUTH.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/BEIJING.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/BERLIN.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/BOCHUM.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/HOF.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/HONG_KONG.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/HONOLULU.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/LAS_VEGAS.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/LONDON.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/LOS_ANGELES.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/MADRID.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/MARKTSCHORGAST.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/MILAN.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/MUNICH.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/NEW_YORK.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/SAN_FRANCISCO.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/SINGAPUR.tf',
'/home/flp/H/mpt/mpt/kernels/fk/targets/SYDNEY.tf',

'/home/flp/H/mpt/mpt/kernels/spk/targets/AACHEN.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/AALEN.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/AREA_51.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/AUCKLAND.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/BARCELONA.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/BAYREUTH.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/BEIJING.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/BERLIN.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/BOCHUM.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/HOF.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/HONG_KONG.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/HONOLULU.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/LAS_VEGAS.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/LONDON.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/LOS_ANGELES.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/MADRID.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/MARKTSCHORGAST.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/MILAN.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/MUNICH.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/NEW_YORK.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/SAN_FRANCISCO.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/SINGAPUR.bsp',
'/home/flp/H/mpt/mpt/kernels/spk/targets/SYDNEY.bsp',


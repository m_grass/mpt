'''
This is a classic hello_world example.
'''

# in the beginning you have to import at least the spiceypy tool (the python version of SPICE) to make use of the SPICE
# functionalities. This program was downloaded beforehand and is already installed in this VM
import spiceypy as cspice
from path_settings.pathsettings import path_setup

# "Unluckily" there is no folder structure with a lot of __init__.py files to easily connect all scripts as this was
# source of some problems during development. Therefore, the path_setup function needs to be executed in every basic
# script to make sure you can use the already existing folder structure. Note: This was a good approach as long as
# everything is stored in one long file. As you might want to write several small programs, make sure the path_setup
# file is the exact same for every of your scripts! A new approach might be useful here...
flp_path = path_setup()


# As we can now use the pathes defined, we do it :P
# To make use of all kernels stored (our database) we need to make it accessible to this script. Therefore, we use our
# first spiceypy routine...
cspice.furnsh(flp_path.mk_out)

# To show a minimal working example, we want to get the distance [km] of the FLP spacecraft with respect to the Earth
# barycenter on the 01. October 2017 at 13:00:00  UTC...

# Specify input parameter:
UTC = '2017 OCT 01 13:00:00.000'  # This is the mostly used time format within this project
observer = 'FLP'  # Vector origin
target = 'EARTH'  # where the vector should point at (barycenter Earth)
frame = 'J2000'  # SPICE frame (coordinate system) the vector should be expressed in. These frames can be designed
# manually and are quite flexible which can be an advantage compared with other programs.
abcorr = 'NONE'  # SPICE can correct for light time and other things. Within this project, no corrections are used.

# Change human-readable UTC to a time format SPICE can compute with(ephemeris time):
ET = cspice.utc2et(UTC)

# Actual command to calculate the relative position. The SPICE default output for this routine is [km].
# The [0] is needed to only get the postion without additional information.
position = cspice.spkpos(target, ET, frame, abcorr, observer)[0]

# Calculate the length of the vector (distance):
distance = cspice.vnorm(position)

# Print to standard output:
print('This is the [x, y, z] position [km] of the ' + observer + ' in ' + frame + ' coordinates: \n' +
      str(position) + '\n\n' +
      'The length of the vector represents the distance [km] between the ' + observer + ' and the ' + target + ': \n' +
      str(distance))



# For more information about the workflow and how to find the write routine, have a look in the master thesis (grass)
# Some time is needed to get familiar with all the structures and hints, eg. np.arrays for positions at multiply times
# seem to be a good solution -> main.py as lookup library to copy-paste code snippets...

# Debugging hello_world
# If an error arises (might be a SPICE error) it is likely that no position data of the observer/target is loaded for
# the time requested. Check your csv/tle files...
# Spelling errors of the frame, target, observer might be another cause so SPICE does not find it in its library...


#--- End of hello_world program ----------------------------




# Checks basic set-ups to see if some kernels need to be downloaded and implemented to the project:

# maybe in a later version.... :D :D
